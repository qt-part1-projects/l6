#include "smileyitem.h"
#include <QPainter>

SmileyItem::SmileyItem()
{
}

QRectF SmileyItem::boundingRect() const {
    QRectF br;
    // enter your code here
    return br;
}

void SmileyItem::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget ) {
    // enter your code here
    // Нарисуйте лицо. Исправьте paintEye так, чтобы использовать правильные координаты и прямоугольник для paintSmile
    paintEye(painter, QPointF());
    paintSmile(painter, QRectF());
}

void SmileyItem::paintEye(QPainter *painter, const QPointF &pt) {
    // enter your code here
    // нарисуйте эллипсы глаз.
}

void SmileyItem::paintSmile(QPainter *painter, const QRectF &rect) {
    // enter your code here
    // нарисуйте дугу улыбки
}
